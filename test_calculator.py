import unittest
from random import randint, random

from calculator import Calculator


class TestCalculator(unittest.TestCase):
    def test_int_sum(self):
        a, b = randint(1, 10), randint(1, 10)
        self.assertEqual(a + b, Calculator.sum(a, b))

    def test_float_sum(self):
        a, b = random(), random()
        self.assertEqual(a + b, Calculator.sum(a, b))

    def test_int_sub(self):
        a, b = randint(1, 10), randint(1, 10)
        self.assertEqual(a - b, Calculator.sub(a, b))

    def test_float_sub(self):
        a, b = random(), random()
        self.assertEqual(a - b, Calculator.sub(a, b))

    def test_int_mul(self):
        a, b = randint(1, 10), randint(1, 10)
        self.assertEqual(a * b, Calculator.mul(a, b))

    def test_float_mul(self):
        a, b = random(), random()
        self.assertEqual(a * b, Calculator.mul(a, b))

    def test_int_div(self):
        a, b = randint(1, 10), randint(1, 10)
        self.assertEqual(a / b, Calculator.div(a, b))

    def test_float_div(self):
        a, b = random() + 0.1, random() + 0.1
        self.assertEqual(a / b, Calculator.div(a, b))

    def test_float_zero_division_raise(self):
        a, b = random(), 0
        with self.assertRaises(ZeroDivisionError):
            Calculator.div(a, b)

if __name__ == '__main__':
    unittest.main()
