import unittest
from unittest.mock import patch

from calculator_view import CalculatorView


class CalculatorViewTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cv = CalculatorView()

    def test_on_sum_clicked(self):
        with patch('__main__.CalculatorView.compute') as mock_method:
            self.cv.builder.get_object('plus_button').clicked()
            mock_method.assert_called()

    def test_on_sub_clicked(self):
        with patch('__main__.CalculatorView.compute') as mock_method:
            self.cv.builder.get_object('minus_button').clicked()
            mock_method.assert_called()

    def test_on_mul_clicked(self):
        with patch('__main__.CalculatorView.compute') as mock_method:
            self.cv.builder.get_object('multiply_button').clicked()
            mock_method.assert_called()

    def test_on_div_clicked(self):
        with patch('__main__.CalculatorView.compute') as mock_method:
            self.cv.builder.get_object('divide_button').clicked()
            mock_method.assert_called()

    def test_answer_compute(self):
        regex = r'.*: 4$'
        self.assertNotRegex(self.cv.answer_label.get_text(), regex)
        self.cv.first_arg_entry.set_text('2')
        self.cv.second_arg_entry.set_text('2')
        self.cv.builder.get_object('plus_button').clicked()
        self.assertRegex(self.cv.answer_label.get_text(), regex)


if __name__ == '__main__':
    unittest.main()
