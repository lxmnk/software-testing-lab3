Feature: sum of two numbers

  	Scenario Outline: check sum work
		Given there are <a> and <b> numbers
		When I press sum button
		Then It should be <result> in result

		Examples:
			|  a  |  b  | result |
		        | 128 | 128 | 256    |
			| 10  | 502 | 512    |
