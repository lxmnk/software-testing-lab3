from behave import *

    
@given('there are {a} and {b} numbers')
def step_impl(context, a, b):
    context.cv.first_arg_entry.set_text(a)
    context.cv.second_arg_entry.set_text(b)


@when('I press sum button')
def step_impl(context):
    context.cv.builder.get_object('plus_button').clicked()


@then('It should be {result} in result')
def step_impl(context, result):
    assert context.cv.answer_label.get_text() == 'Result: {0}'.format(result)
