import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from calculator import Calculator


class CalculatorView:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('calculator.glade')
        self.first_arg_entry = self.builder.get_object('first_arg_entry')
        self.second_arg_entry = self.builder.get_object('second_arg_entry')
        self.answer_label = self.builder.get_object('answer_label')
        handlers = {'on_sum_clicked': lambda x: self.compute(Calculator.sum),
                    'on_sub_clicked': lambda x: self.compute(Calculator.sub),
                    'on_mul_clicked': lambda x: self.compute(Calculator.mul),
                    'on_div_clicked': lambda x: self.compute(Calculator.div),
                    'gtk_main_quit': lambda x, *args: Gtk.main_quit(*args)}
        self.builder.connect_signals(handlers)
        self.builder.get_object('calculator_window').show_all()

    def compute(self, func):
        first_arg = self.get_first_argument()
        second_arg = self.get_second_argument()
        try:
            result = func(float(first_arg), float(second_arg))
        except ZeroDivisionError as error:
            self.display_error(str(error))
        else:
            if result.is_integer():
                result = int(result)
            self.print_result(str(result))

    def display_error(self, error):
        self._write_to_answer_label('Error! {0}!'.format(error.capitalize()))

    def get_first_argument(self):
        return self.first_arg_entry.get_text()

    def get_second_argument(self):
        return self.second_arg_entry.get_text()

    def gtk_main_quit(self, *args):
        Gtk.main_quit(*args)

    def print_result(self, result):
        self._write_to_answer_label('Result: {0}'.format(result))

    def _write_to_answer_label(self, text):
        self.answer_label.set_label(text)


if __name__ == '__main__':
    cv = CalculatorView()
    Gtk.main()
